package by.training.jsptagtask;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Class for output of all servlet and mapping.
 */
public class ListMapping extends TagSupport {
    /**
     * Contain mapping as key and servlet as value.
     */
    private Map<String, String> registrations = new HashMap<String, String>();
    /**
     * Last printed mapping.
     */
    private String lastKey;

    public Map<String, String> getRegistrations() {
        return registrations;
    }

    public void setRegistrations(Map<String, String> registrations) {
        this.registrations = registrations;
    }

    public String getLastKey() {
        return lastKey;
    }

    public void setLastKey(String lastKey) {
        this.lastKey = lastKey;
    }

    /**
     * Get all servlet and mapping and fill @registrations.
     */
    private void getURLMapping() {
        ServletContext servletContext = pageContext.getServletContext();
        Map<String, ? extends ServletRegistration> mapRegistration = servletContext.getServletRegistrations();

        for (String servletName : mapRegistration.keySet()
                ) {
            ServletRegistration servletRegistration = servletContext.getServletRegistration(servletName);
            java.util.Collection<java.lang.String> mappings = servletRegistration.getMappings();
            for (String servletMapping : mappings
                    ) {
                getRegistrations().put(servletMapping, servletName);
            }
        }
    }

    /***
     * Set one pair servlet and mapping for output.
     * Remove pair form map.
     */
    private void setAttributeURLMapping() {
        Map.Entry<String, String> map = getRegistrations().entrySet().iterator().next();
        pageContext.setAttribute("servlet", map.getKey());
        pageContext.setAttribute("url", map.getValue());
        setLastKey(map.getKey());

        getRegistrations().remove(getLastKey());
    }

    /**
     * Get information about servlet and mapping.
     * Output first pair.
     *
     * @return .
     */
    @Override
    public int doStartTag() {

        getURLMapping();

        if (!getRegistrations().isEmpty()) {
            setAttributeURLMapping();
        }

        return EVAL_BODY_INCLUDE;
    }

    /***
     * Output servlet name and mapping if map not empty.
     * @return .
     * @throws JspException .
     */
    @Override
    public int doAfterBody() throws JspException {
        if (!getRegistrations().isEmpty()) {

            setAttributeURLMapping();

            try {
                pageContext.getOut().print("<br>");
            } catch (IOException e) {
                throw new JspException(e.toString());
            }
            return EVAL_BODY_AGAIN;
        } else {
            return SKIP_BODY;
        }
    }
}
