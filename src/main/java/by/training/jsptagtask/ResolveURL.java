package by.training.jsptagtask;

import javax.servlet.ServletContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import javax.servlet.jsp.PageContext;
import java.io.IOException;

/**
 * Class for output URL and real path on server.
 */
public class ResolveURL extends SimpleTagSupport {

    /**
     * URL for resolve.
     */
    private String url = "";

    /**
     * Set URL from jsp.
     *
     * @param url
     */
    public void setUrl(final String url) {
        this.url = url;
    }


    /**
     * Print servlet URL and real path.
     *
     * @throws IOException  .
     * @throws JspException .
     */
    public void doTag() throws IOException, JspException {
        PageContext pageContext = (PageContext) getJspContext();
        ServletContext servletContext = pageContext.getServletContext();

        this.getJspContext().setAttribute("url", url);
        this.getJspContext().setAttribute("resource", servletContext.getRealPath(url));
        try {
            this.getJspBody().invoke(null);
        } catch (JspException e) {
            throw new JspException(e.toString());
        } catch (IOException e) {
            throw new IOException(e.toString());
        }

    }
}
