package by.training.jsptagtask;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Simple first servlet for task.
 */
public class Servlet extends HttpServlet {

    /**
     * Servlet body.
     */
    private String responseTemplate =
            "<html>\n"
                    + "<body>\n"
                    + "<h2>First Servlet</h2>\n"
                    + "</body>\n"
                    + "</html>";
    /**
     * Status 200 - OK.
     */
    private final int successStatus = 200;

    /**
     * Overrided doGet for GET.
     * @param request
     * @param response
     * @throws IOException
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        this.process(request, response);
    }

    /**
     * Overrided doGet for POST.
     * @param request
     * @param response
     * @throws IOException
     */
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        this.process(request, response);
    }

    /**
     * Write servlet body to response writer.
     * @param request .
     * @param response .
     * @throws IOException .
     */
    private void process(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        response.setStatus(successStatus);
        response.getWriter().write(responseTemplate);
    }
}
