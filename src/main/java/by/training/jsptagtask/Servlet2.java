package by.training.jsptagtask;

/**
 * Simple second servlet for task.
 */
public class Servlet2 extends Servlet {

    /**
     * Servlet body.
     */
    private String responseTemplate =
            "<html>\n"
                    + "<body>\n"
                    + "<h2>Second Servlet</h2>\n"
                    + "</body>\n"
                    + "</html>";
}
