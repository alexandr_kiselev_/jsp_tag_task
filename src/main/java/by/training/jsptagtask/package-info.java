/**
 * JSP tag API task contains classic and simple tag realisation.
 *
 * @author Alexandr_Kiselev
 * @version 1.0
 * @since 1.0
 */
package by.training.jsptagtask;