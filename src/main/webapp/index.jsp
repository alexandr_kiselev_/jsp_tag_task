<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="utils" uri="http://utils.tag" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title> JSP tag API task</title>
</head>
<body>
<h1>Classic tag API </h1>
<utils:listmapping>
    ${url} - ${servlet}
</utils:listmapping>
<h1>Simple tag API </h1>
<%
    pageContext.setAttribute("url_to_test", "/servlet2");
%>
<utils:resolveurl url="${url_to_test}">
    ${url} - ${resource}
</utils:resolveurl>
</body>
</html>